package com.example.nashnyccodechallenge.network

import com.example.nashnyccodechallenge.models.NYCSchoolModel
import retrofit2.http.GET

interface NYCSchoolsApi {
    @GET("s3k6-pzi2.json")
    suspend fun getAvailableSchools(): List<NYCSchoolModel>
}