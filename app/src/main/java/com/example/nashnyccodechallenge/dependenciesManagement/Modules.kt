package com.example.nashnyccodechallenge.dependenciesManagement

import com.example.nashnyccodechallenge.BuildConfig
import com.example.nashnyccodechallenge.network.NYCSchoolsApi
import com.example.nashnyccodechallenge.repositories.SchoolsRepositoryContract
import com.example.nashnyccodechallenge.repositories.SchoolsRepositoryV1
import com.example.nashnyccodechallenge.ui.screens.details.DetailsViewModel
import com.example.nashnyccodechallenge.ui.screens.main.SchoolsScreenViewModel
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL: String = "https://data.cityofnewyork.us/resource/"

val appModule = module {
    factory<Interceptor> {
        HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        }
    }
    factory<OkHttpClient> {
        OkHttpClient
            .Builder()
            .addInterceptor(get<Interceptor>())
            .build()
    }
    factory<Retrofit> {
        Retrofit
            .Builder()
            .baseUrl(BASE_URL)
            .client(get())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    factory<NYCSchoolsApi> { get<Retrofit>().create(NYCSchoolsApi::class.java) }
    single<SchoolsRepositoryContract> { SchoolsRepositoryV1(get()) }
    viewModel { SchoolsScreenViewModel(get()) }
    viewModel { DetailsViewModel(get()) }
}