package com.example.nashnyccodechallenge

import android.app.Application
import com.example.nashnyccodechallenge.dependenciesManagement.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class NYCSchoolsApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@NYCSchoolsApp)
            modules(appModule)
        }
    }
}