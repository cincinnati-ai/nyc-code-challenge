package com.example.nashnyccodechallenge.ui.screens.errors

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.nashnyccodechallenge.ui.NavRoutes
import com.example.nashnyccodechallenge.R
import com.example.nashnyccodechallenge.ui.components.baseScreenModifier

@Composable
fun ErrorScreen(navController: NavController? = null) {
    val errorMessage = stringResource(R.string.text_error)
    val roundedCornerShape = RoundedCornerShape(24.dp)
    Column(
        modifier = baseScreenModifier
    ) {
        Image(
            painterResource(id = R.drawable.nyc_flag_drawable),
            contentDescription = errorMessage,
            modifier = Modifier
                .size(200.dp)
                .align(Alignment.CenterHorizontally)
        )
        Text(text = errorMessage, fontWeight = FontWeight.Bold)

        Text(
            text = "Try again!",
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(64.dp)
                .background(Color.White, roundedCornerShape)
                .border(Dp.Hairline, Color.Black, roundedCornerShape)
                .background(Color.White)
                .padding(12.dp)
                .clickable {
                    navController?.navigate(NavRoutes.HOME.route)
                }
        )
    }
}

@Preview
@Composable
fun PreviewErrorScreen() {
    ErrorScreen()
}