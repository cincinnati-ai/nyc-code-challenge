package com.example.nashnyccodechallenge.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.nashnyccodechallenge.R

@Composable
fun LoadingProgressIndicator() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Gray)
            .padding(32.dp)
    ) {
        Image(
            painterResource(id = R.drawable.ic_school),
            contentDescription = "Loading",
            modifier = Modifier.size(200.dp)
        )
        Text(text = stringResource(R.string.text_loading_schools))
    }
}

@Preview
@Composable
fun LoadingSchoolsSignPreview() {
    LoadingProgressIndicator()
}