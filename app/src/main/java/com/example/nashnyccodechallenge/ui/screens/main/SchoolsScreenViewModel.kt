package com.example.nashnyccodechallenge.ui.screens.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nashnyccodechallenge.models.NYCSchoolModel
import com.example.nashnyccodechallenge.models.SchoolsScreenState
import com.example.nashnyccodechallenge.repositories.SchoolsRepositoryContract
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class SchoolsScreenViewModel(
    private val schoolsRepository: SchoolsRepositoryContract
) : ViewModel() {

    val schoolsState: StateFlow<SchoolsScreenState> by lazy { _schoolsState }
    private val _schoolsState: MutableStateFlow<SchoolsScreenState> =
        MutableStateFlow(SchoolsScreenState.None)

    fun getAllSchools() {
        _schoolsState.value = SchoolsScreenState.Loading
        viewModelScope.launch {
            try {
                val response = schoolsRepository.getAllSchools()
                if (response.isEmpty()) {
                    _schoolsState.emit(SchoolsScreenState.EmptySuccess)
                } else {
                    _schoolsState.emit(SchoolsScreenState.Success(response))
                }
            } catch (e: Exception) {
                _schoolsState.emit(SchoolsScreenState.Failure(e.localizedMessage.toString()))
            }
        }
    }

    fun saveSchoolForDetails(school: NYCSchoolModel) {
        schoolsRepository.persistSelectedSchool(school)
    }
}