package com.example.nashnyccodechallenge.ui.screens.main

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.navigation.NavHostController
import com.example.nashnyccodechallenge.models.NYCSchoolModel
import com.example.nashnyccodechallenge.models.SchoolsScreenState
import com.example.nashnyccodechallenge.ui.NavRoutes
import com.example.nashnyccodechallenge.ui.components.LoadingProgressIndicator
import com.example.nashnyccodechallenge.ui.components.SchoolCard
import com.example.nashnyccodechallenge.ui.components.baseScreenModifier

@Composable
fun SchoolsScreen(viewModel: SchoolsScreenViewModel, navController: NavHostController) {

    val schoolsState = viewModel.schoolsState

    LaunchedEffect(Unit) {
        viewModel.getAllSchools()
    }

    when (val state = schoolsState.collectAsState().value) {

        is SchoolsScreenState.Failure, SchoolsScreenState.EmptySuccess -> {
            navController.navigate(NavRoutes.ERROR.route)
        }

        is SchoolsScreenState.Loading, SchoolsScreenState.None -> {
            LoadingProgressIndicator()
        }

        is SchoolsScreenState.Success -> DisplayAllSchools(state.schools) {
            viewModel.saveSchoolForDetails(it)
            navController.navigate(NavRoutes.DETAILS.route)
        }
    }
}

@Composable
fun DisplayAllSchools(schools: List<NYCSchoolModel>, onClick: (NYCSchoolModel) -> Unit) {
    LazyColumn(
        modifier = baseScreenModifier
    ) {
        items(schools) { school ->
            SchoolCard(school) {
                onClick(school)
            }
        }
    }
}
