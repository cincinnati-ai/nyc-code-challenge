package com.example.nashnyccodechallenge.ui

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.nashnyccodechallenge.ui.screens.details.DetailsScreen
import com.example.nashnyccodechallenge.ui.screens.details.DetailsViewModel
import com.example.nashnyccodechallenge.ui.screens.errors.ErrorScreen
import com.example.nashnyccodechallenge.ui.screens.main.SchoolsScreen
import com.example.nashnyccodechallenge.ui.screens.main.SchoolsScreenViewModel
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val schoolsViewModel: SchoolsScreenViewModel by inject()
    private val detailsViewModel: DetailsViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val navController = rememberNavController()

            NavHost(
                navController = navController,
                startDestination = "home"
            ) {
                composable(NavRoutes.HOME.route) { SchoolsScreen(schoolsViewModel, navController) }
                composable(NavRoutes.DETAILS.route) { DetailsScreen(detailsViewModel) }
                composable(NavRoutes.ERROR.route) { ErrorScreen(navController) }
            }
        }
    }
}

enum class NavRoutes(val route: String) {
    HOME("home"),
    DETAILS("details"),
    ERROR("error")
}