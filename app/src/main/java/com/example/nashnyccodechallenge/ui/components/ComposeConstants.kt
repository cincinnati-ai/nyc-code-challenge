package com.example.nashnyccodechallenge.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

val baseScreenModifier by lazy {
    Modifier
        .fillMaxSize()
        .background(Color.White)
        .padding(32.dp)
}
