package com.example.nashnyccodechallenge.ui.screens.details

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.nashnyccodechallenge.ui.components.baseScreenModifier

@Composable
fun DetailsScreen(viewModel: DetailsViewModel) {

    LaunchedEffect(Unit) {
        viewModel.getLastSelectedSchool()
    }

    val data = viewModel.schoolToDisplay.collectAsState().value
    if (data != null) {
        Column(
            modifier = baseScreenModifier
        ) {
            val textsModifier = Modifier
                .padding(horizontal = 12.dp, vertical = 32.dp)
            Text(text = data.schoolName, textsModifier)
            Text(text = data.databaseId, textsModifier)
            Text(text = data.overviewText, textsModifier)
        }
    }
}