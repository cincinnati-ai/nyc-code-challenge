package com.example.nashnyccodechallenge.ui.screens.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nashnyccodechallenge.models.NYCSchoolModel
import com.example.nashnyccodechallenge.repositories.SchoolsRepositoryContract
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class DetailsViewModel(
    private val schoolsRepository: SchoolsRepositoryContract
) : ViewModel() {

    private val _schoolToDisplay = MutableStateFlow<NYCSchoolModel?>(null)
    val schoolToDisplay: StateFlow<NYCSchoolModel?> by lazy { _schoolToDisplay }

    fun getLastSelectedSchool() {
        viewModelScope.launch {
            val school = schoolsRepository.fetchLastSelectedSchool()
            _schoolToDisplay.emit(school)
        }
    }
}