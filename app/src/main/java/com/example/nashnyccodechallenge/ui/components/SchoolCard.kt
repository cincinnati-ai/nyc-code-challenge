package com.example.nashnyccodechallenge.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.nashnyccodechallenge.models.NYCSchoolModel

@Composable
fun SchoolCard(
    school: NYCSchoolModel,
    onClick: () -> Unit = {}
) {
    val roundedCornerShape = RoundedCornerShape(12.dp)

    Column(
        modifier = Modifier
            .padding(8.dp)
            .background(Color.White, roundedCornerShape)
            .border(Dp.Hairline, Color.Black, roundedCornerShape)
            .background(Color.White)
            .padding(4.dp)
            .clickable {
                onClick()
            }
    ) {
        val textModifier = Modifier.padding(vertical = 4.dp)
        Text(
            text = "School id: ${school.databaseId}",
            color = Color.Black,
            modifier = textModifier
        )
        Text(
            text = "School name: ${school.schoolName}",
            color = Color.Black,
            modifier = textModifier
        )
    }
}

@Preview
@Composable
fun PreviewSchoolCard() {
    val school = NYCSchoolModel(
        databaseId = "someId",
        schoolName = "Some Awesome School",
        overviewText = "Visit us!"
    )
    SchoolCard(school)
}