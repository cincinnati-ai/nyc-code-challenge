package com.example.nashnyccodechallenge.repositories

import com.example.nashnyccodechallenge.models.NYCSchoolModel
import com.example.nashnyccodechallenge.network.NYCSchoolsApi
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.annotations.VisibleForTesting

class SchoolsRepositoryV1(
    private val api: NYCSchoolsApi,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : SchoolsRepositoryContract {

    @VisibleForTesting
    val availableSchools = mutableListOf<NYCSchoolModel>()

    @VisibleForTesting
    var selectedSchool: NYCSchoolModel? = null

    // TODO: Add a proper persistence mechanism like ROOM or SQLite.
    // TODO: Add timestamp verification to ensure the right data is available and not stale data.
    override suspend fun getAllSchools(): List<NYCSchoolModel> {
        if (availableSchools.isEmpty()) {
            return withContext(dispatcher) {
                val newSchools = api.getAvailableSchools()
                availableSchools.addAll(newSchools)
                return@withContext availableSchools
            }
        } else {
            return availableSchools
        }
    }

    // TODO: Add a proper persistence mechanism like SharedPreferences.
    override fun fetchLastSelectedSchool(): NYCSchoolModel? {
        return selectedSchool
    }

    override fun persistSelectedSchool(school: NYCSchoolModel) {
        selectedSchool = school
    }
}