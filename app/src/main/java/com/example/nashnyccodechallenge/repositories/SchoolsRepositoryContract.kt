package com.example.nashnyccodechallenge.repositories

import com.example.nashnyccodechallenge.models.NYCSchoolModel

interface SchoolsRepositoryContract {
    suspend fun getAllSchools(): List<NYCSchoolModel>
    fun fetchLastSelectedSchool(): NYCSchoolModel?
    fun persistSelectedSchool(school: NYCSchoolModel)
}