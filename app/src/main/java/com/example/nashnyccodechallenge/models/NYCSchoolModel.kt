package com.example.nashnyccodechallenge.models

import com.google.gson.annotations.SerializedName

data class NYCSchoolModel(
    @SerializedName("dbn")
    val databaseId: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("overview_paragraph")
    val overviewText: String
)
