package com.example.nashnyccodechallenge.models

sealed class SchoolsScreenState {
    data class Success(val schools: List<NYCSchoolModel>) : SchoolsScreenState()
    object EmptySuccess : SchoolsScreenState()
    data class Failure(val message: String) : SchoolsScreenState()
    object Loading : SchoolsScreenState()
    object None : SchoolsScreenState()
}