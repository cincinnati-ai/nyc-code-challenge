package com.example.nashnyccodechallenge.dependencies

import com.example.nashnyccodechallenge.dependenciesManagement.appModule
import org.junit.Test
import org.koin.core.annotation.KoinExperimentalAPI
import org.koin.test.KoinTest
import org.koin.test.verify.verify

class CheckModulesTest : KoinTest {

    @OptIn(KoinExperimentalAPI::class)
    @Test
    fun `verify all Koin modules load correctly`() {
        appModule.verify()
    }
}