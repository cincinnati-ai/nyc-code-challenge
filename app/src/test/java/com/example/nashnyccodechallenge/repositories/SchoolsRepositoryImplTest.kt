package com.example.nashnyccodechallenge.repositories

import com.example.nashnyccodechallenge.models.NYCSchoolModel
import com.example.nashnyccodechallenge.network.NYCSchoolsApi
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolsRepositoryImplTest {

    private val mockSchool1 = mockk<NYCSchoolModel>()
    private val mockSchool2 = mockk<NYCSchoolModel>()

    @Test
    fun getAllSchools_returnsCachedSchoolsIfAvailable() = runBlockingTest {
        // Arrange
        val mockApi = mockk<NYCSchoolsApi>()
        val repository = SchoolsRepositoryV1(mockApi)

        val expectedSchools = listOf(mockSchool1, mockSchool2)
        repository.availableSchools.clear()
        repository.availableSchools.addAll(expectedSchools)

        // Act
        val result = repository.getAllSchools()

        // Assert
        assertThat(result).isEqualTo(expectedSchools)
        coVerify(exactly = 0) { mockApi.getAvailableSchools() }
    }

    @Test
    fun getAllSchools_fetchesNewSchoolsIfCacheEmpty() = runBlockingTest {
        // Arrange
        val mockApi = mockk<NYCSchoolsApi>()
        val expectedSchools = listOf(mockSchool1, mockSchool2)
        coEvery { mockApi.getAvailableSchools() } returns expectedSchools
        val repository = SchoolsRepositoryV1(mockApi, dispatcher = Dispatchers.Unconfined)

        // Act
        val result = repository.getAllSchools()

        // Assert
        assertThat(result).isEqualTo(expectedSchools)
        assertThat(repository.availableSchools).isEqualTo(expectedSchools)
        coVerify(exactly = 1) { mockApi.getAvailableSchools() }
    }

    @Test
    fun getLastSelectedSchool_returnsSavedSchool() {
        // Arrange
        val repository = SchoolsRepositoryV1(mockk())
        repository.selectedSchool = mockSchool2

        // Act
        val result = repository.fetchLastSelectedSchool()

        // Assert
        assertThat(result).isEqualTo(mockSchool2)
    }

    @Test
    fun saveSelectedSchool_storesSchool() {
        // Arrange
        val repository = SchoolsRepositoryV1(mockk())
        val schoolToSave = mockSchool1

        // Act
        repository.persistSelectedSchool(schoolToSave)

        // Assert
        assertThat(repository.selectedSchool).isEqualTo(schoolToSave)
    }
}