package com.example.nashnyccodechallenge.screens

import com.example.nashnyccodechallenge.models.SchoolsScreenState
import com.example.nashnyccodechallenge.repositories.SchoolsRepositoryContract
import com.example.nashnyccodechallenge.ui.screens.main.SchoolsScreenViewModel
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class SchoolsScreenViewModelTest {

    @Before
    fun setup() {
        Dispatchers.setMain(StandardTestDispatcher())
    }

    @Test
    fun getAllSchools_setsLoadingStateBeforeFetching() = runBlockingTest {
        val mockRepository = mockk<SchoolsRepositoryContract>()
        val viewModel = SchoolsScreenViewModel(mockRepository)

        viewModel.getAllSchools()

        coVerify(exactly = 0) { mockRepository.getAllSchools() }
        assertThat(viewModel.schoolsState.value).isEqualTo(SchoolsScreenState.Loading)
    }

    @Test
    fun getAllSchools_emitsEmptySuccessOnEmptyResponse() = runBlockingTest {
        val mockRepository = mockk<SchoolsRepositoryContract>()
        coEvery { mockRepository.getAllSchools() } returns emptyList() // Set up mock to return empty list

        val viewModel = SchoolsScreenViewModel(mockRepository)
        val states = mutableListOf<SchoolsScreenState>()
        viewModel.schoolsState.collect {
            states.add(it)
        }

        advanceUntilIdle()
        assertThat(states.first()).isEqualTo(SchoolsScreenState.EmptySuccess)

        coVerify { mockRepository.getAllSchools() }
    }
}