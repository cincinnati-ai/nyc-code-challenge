### Nashely Chan's Code Challenge

Requirements:

Create a native app in Kotlin with Jetpack Compose components to provide information on NYC High schools.
- Display a list of NYC High Schools (school_name, dbn)
- Access the JSON API directly from below link: z
- Selecting a school should show additional information about the school. At the very least, display the overview_paragraph
- Please implement using MVVM architecture to solve #1 and #2
- Implement the unit test cases using JUnit/Mockito
- Write automation will be plus point.
- Ensure that you provide clear and thorough explanations while writing your code. Clarify the steps and reasoning behind your decisions, making it easier for others to understand and learn from your work.


Code Submission Guidelines:
1.      Submit your code via Gitlab.com. Feel free to use your current account or create a free account.
2.      Create a new PUBLIC project.
3.      Create a repository and add your code challenge.
4.      Add the email address of Photon Point of Contact as a “Master” to your project:
User Name: Shyam-Photon/ Photon PoC email ids: Shyam.su1@photon.com
4. Also share your Public project link or entire code Repo Zip file to the recruiter
   The DevSecOps Platform
   From planning to production, bring teams together in one application. Ship secure code more efficiently to deliver value faster.


